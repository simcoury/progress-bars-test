var APP = angular.module('APP', []);

var CONFIG = {
    endPointUrl: "//pb-api.herokuapp.com/bars",
    prefixId: "progress"
};

APP.service('commonServices', function () {

    this.uniqSortValues = function (arr) {
        //button values clean up, as per initial testing can see some duplicate numbers
        //checking for unique values ONLY
        arr.filter(function (value, index) {
            return arr.indexOf(value) == index
        });

        //sorting the array/values by the numbers
        arr.sort(function (a, b) {
            return a - b;
        });

        return arr;
    }; //end uniqSortValues

    this.getProgressBarUI = function (arr, limit) {
        var bars = [];
        angular.forEach(arr, function (value, key) {
            //defining progress bar properties
            bars.push({
                id: CONFIG.prefixId + (key + 1),
                selected: (key == 0),
                defaultValue: value,
                currentValue: value,
                percentageValue: Math.round((value / limit) * 100)
            });
        });
        return bars;
    }; //end getProgressBarUI

    this.getControlButtons = function (arr) {
        var buttons = [];
        angular.forEach(arr, function (value, key) {
            buttons.push({
                value: value,
                display: Math.abs(value),
                iconText: value > 0 ? "plus" : "minus"
            });
        });
        return buttons;
    }; //end getControlButtons

    this.calculateProgress = function (progressBars, selectedBarId, value, limit) {
        //find the object based on the selected progress bar ID
        var findr = progressBars.filter(function (item) {
            return item.id == selectedBarId;
        })[0];

        //calculate the current value and its equivalent percentage based on the limit initiatilly set
        findr.currentValue = findr.currentValue + value;
        findr.percentageValue = Math.round((findr.currentValue / limit) * 100);
        findr.message = "";

        //if the actual value is negative, set the values to 0
        if (findr.currentValue < 0) {
            findr.currentValue = 0;
            findr.percentageValue = 0;
            findr.message = "NEGATIVE-VALUES";
        }

        return findr;
    }; //end calculateProgress

}); //end commonServices

APP.controller('mainController', function (commonServices, $scope, $http) {

    $scope.selectedBarId = "";
    $scope.ctrlButtons = [];
    $scope.progressBars = [];
    $scope.limitValue = 0;
    $scope.resetObjects = {};

    $scope.init = function () {
        $http.get(CONFIG.endPointUrl).then(function (resp) {
            if (typeof resp !== "undefined") {
                if (typeof resp.data !== "undefined") {
                    $scope.setupUI(resp.data);
                }
            }
        });
    } //end init function

    $scope.setupUI = function (params) {

        //set the limit
        $scope.limitValue = params.limit;

        //set selected/default progress id
        $scope.selectedBarId = CONFIG.prefixId + "1";

        //propagate progress bars depending on the API response
        $scope.progressBars = commonServices.getProgressBarUI(params.bars, params.limit);

        //propagate control buttons
        $scope.ctrlButtons = commonServices.getControlButtons(commonServices.uniqSortValues(params.buttons));

        $scope.resetObjects = {
            selectedBarId: $scope.selectedBarId,
            progressBars: JSON.stringify($scope.progressBars)
        };
    }; //end setupUI function

    $scope.updateProgressBar = function (value) {
        commonServices.calculateProgress(
                $scope.progressBars,
                $scope.selectedBarId,
                value,
                $scope.limitValue
                );
    }; //end updateProgressBar function

    //optional function to resel the original values of the progress bar
    $scope.resetProgressBar = function () {

//                angular.forEach($scope.resetObjects, function (objValues, key) {
//                    if (key == "progressBars") {
//                        objValues = JSON.parse(objValues);
//                    }
//                    $scope[key] = objValues;
//                });

        $scope.selectedBarId = $scope.resetObjects['selectedBarId'];

        var sProgressBars = JSON.parse($scope.resetObjects['progressBars']);
        angular.forEach(sProgressBars, function (objValues, key) {
            $scope.progressBars[key] = objValues;
        });
    }; //end resetProgressBar function

});
