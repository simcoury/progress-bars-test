APP.controller('test', function (commonServices, $scope, $http) {

    $scope.messages = [];

    $scope.sampleData = {"buttons": [13, 8, -32, -9], "bars": [89, 69, 52, 12], "limit": 110};
    $scope.progressBarObjects;
    $scope.ctrlButtons;

    $scope.updateProgressTestCtr = 1;
    $scope.testCasesResults = {
        passed: 0,
        failed: 0
    };

    $scope.init = function () {

        $scope.messages.push("Testing started...");

        $scope.messages.push("Sample Data: " + JSON.stringify($scope.sampleData));

        $scope.progressBarObjects = commonServices.getProgressBarUI($scope.sampleData.bars, $scope.sampleData.limit);
        $scope.ctrlButtons = commonServices.getControlButtons(commonServices.uniqSortValues($scope.sampleData.buttons));
        $scope.messages.push("No. of Progress Bars: " + $scope.progressBarObjects.length +
                "; No. of Control Buttons: " + $scope.ctrlButtons.length);

        //run test cases to calculate/update progress bar
        $scope.updateProgressTest(0, 0);
        $scope.updateProgressTest(0, 3);
        $scope.updateProgressTest(1, 2);
        $scope.updateProgressTest(1, 3);

        $scope.messages.push("No. of tests ran: " + $scope.updateProgressTestCtr);
        $scope.messages.push(JSON.stringify($scope.testCasesResults));

    }; //end init function

    $scope.updateProgressTest = function (progressBarIndex, ctrlButtonIndex) {

        $scope.messages.push("Test #" + $scope.updateProgressTestCtr + ": Updating Progress Bar ID " +
                $scope.progressBarObjects[progressBarIndex].id +
                " / Button Index: " + ctrlButtonIndex);

        $scope.messages.push(" - Initial Object: " + JSON.stringify($scope.progressBarObjects[progressBarIndex]));
        var tmpExpectedRes1 = $scope.progressBarObjects[progressBarIndex].currentValue + $scope.ctrlButtons[ctrlButtonIndex].value;

        $scope.messages.push(" - Expected Result: " + $scope.progressBarObjects[progressBarIndex].id + " (value: " +
                $scope.progressBarObjects[progressBarIndex].currentValue + " + " + $scope.ctrlButtons[ctrlButtonIndex].value + " = " + tmpExpectedRes1 + ")");

        var selectedProgressBarObject = commonServices.calculateProgress(
                $scope.progressBarObjects,
                $scope.progressBarObjects[progressBarIndex].id,
                $scope.ctrlButtons[ctrlButtonIndex].value,
                $scope.sampleData.limit
                );

        $scope.messages.push(" - Updated Object: " + JSON.stringify(selectedProgressBarObject));
        $scope.messages.push(" - Result: " + tmpExpectedRes1 + "==" + selectedProgressBarObject.currentValue +
                " => " + (tmpExpectedRes1 === selectedProgressBarObject.currentValue));

        if (tmpExpectedRes1 === selectedProgressBarObject.currentValue) {
            $scope.testCasesResults.passed++;
        } else {
            $scope.testCasesResults.failed++;
        }

        $scope.updateProgressTestCtr++;
    }; //end updateProgressTest function

});
